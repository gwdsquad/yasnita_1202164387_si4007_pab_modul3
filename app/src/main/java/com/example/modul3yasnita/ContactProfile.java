package com.example.modul3yasnita;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactProfile extends AppCompatActivity {
    private TextView profileNama, profilePekerjaan;
    private ImageView profilePhoto;
    private int imgPhoto;
    private String tvNama, tvPekerjaan;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_contact);

        profileNama = findViewById(R.id.tvNama);
        profilePekerjaan = findViewById(R.id.tvPekerjaan);
        profilePhoto = findViewById(R.id.imgPhoto);

        tvNama = getIntent().getStringExtra("nama");
        tvPekerjaan = getIntent().getStringExtra("pekerjaan");
        imgPhoto = getIntent().getIntExtra("jeniskelamin", 2);

        profileNama.setText(tvNama);
        profilePekerjaan.setText(tvPekerjaan);


        switch (imgPhoto){
            case 1:
                profilePhoto.setImageResource(R.drawable.ic_male);
                break;
            case 2:
            default:
                profilePhoto.setImageResource(R.drawable.ic_female);
                break;
        }


    }


}
