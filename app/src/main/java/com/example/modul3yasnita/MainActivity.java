package com.example.modul3yasnita;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private ArrayList<ContactList> mContactList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ContactListAdapter mAdapter;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.rView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mContactList = new ArrayList<>();
        if (savedInstanceState != null) {
            mContactList.clear();
            for (int i = 0; i < savedInstanceState.getStringArrayList("nama").size(); i++) {
                mContactList.add(new ContactList(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("pekerjaan").get(i),
                        savedInstanceState.getIntegerArrayList("jk").get(i)));
            }
        } else {
            init();
        }
        mAdapter = new ContactListAdapter(mContactList, this);
        mRecyclerView.setAdapter(mAdapter);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                DialogForm();
            }
        });
    }


    private void init() {
        mContactList.clear();
        mContactList.add(new ContactList("Lala", "Developer", 1));
    }


    private void DialogForm() {
        dialog = new AlertDialog.Builder(MainActivity.this);

        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.form_dialog, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("Biodata");
        final TextView txt_nama, txt_pekerjaan;
        final Spinner txt_jk;

        txt_nama = (EditText) dialogView.findViewById(R.id.txt_nama);
        txt_pekerjaan = (EditText) dialogView.findViewById(R.id.txt_pekerjaan);
        txt_jk = dialogView.findViewById(R.id.txt_jk);



        String[] List = {"Male", "Female"};
        ArrayAdapter<String> adapterX = new ArrayAdapter(dialog.getContext(), android.R.layout.simple_spinner_item, List);
        txt_jk.setAdapter(adapterX);

        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mContactList.add(new ContactList(txt_nama.getText().toString(), txt_pekerjaan.getText().toString(), txt_jk.getSelectedItemPosition() + 1));
                mAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ArrayList<String> tempListNama = new ArrayList<>();
        ArrayList<String> tempListPekerjaan = new ArrayList<>();
        ArrayList<String> tempListJk = new ArrayList<>();
        for (int i = 0; i < mContactList.size(); i++) {
            tempListNama.add(mContactList.get(i).getNama());
            tempListPekerjaan.add(mContactList.get(i).getPekerjaan());
            //tempListJk.add(mContactList.get(i).getPhoto());
        }

        outState.putStringArrayList("nama", tempListNama);
        outState.putStringArrayList("pekerjaan", tempListPekerjaan);
        outState.putStringArrayList("Jenis Kelamin", tempListJk);
        super.onSaveInstanceState(outState);
    }
}



