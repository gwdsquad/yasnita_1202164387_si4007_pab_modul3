package com.example.modul3yasnita;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;


public class ContactListAdapter extends
        RecyclerView.Adapter<ContactListAdapter.ContactViewHolder> {

    private ArrayList<ContactList> mContactList;
    private Context mContext;

    class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView nama, pekerjaan;
        private ImageView photo;
        private int avatarCode;

        public ContactViewHolder(View itemView) {
            //ContactListAdapter adapter) {
            super(itemView);

            nama = itemView.findViewById(R.id.txt_nama);
            pekerjaan = itemView.findViewById(R.id.txt_pekerjaan);
            photo = itemView.findViewById(R.id.cardPerson);

            itemView.setOnClickListener(this);
        }


        public void bindTo(ContactList mCurrent) {
            nama.setText(mCurrent.getNama());
            pekerjaan.setText(mCurrent.getPekerjaan());
            //photo.setImageResource(mCurrent.getAvatar());

            avatarCode = mCurrent.getPhoto();
            switch (mCurrent.getPhoto()) {
                case 1:
                    photo.setImageResource(R.drawable.ic_male);
                    break;
                case 2:
                default:
                    photo.setImageResource(R.drawable.ic_male);
            }
        }
        @Override
        public void onClick(View view) {
            Intent toContactProfile = new Intent(view.getContext(), ContactProfile.class);
            toContactProfile.putExtra("nama", nama.getText().toString());
            toContactProfile.putExtra("jeniskelamin", avatarCode);
            toContactProfile.putExtra("pekerjaan", pekerjaan.getText().toString());
            view.getContext().startActivity(toContactProfile);
        }
    }


    public ContactListAdapter(ArrayList<ContactList> ContactList, Context mContext){
        this.mContext = mContext;
        this.mContactList = mContactList;
    }

    @Override
    public ContactListAdapter.ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View itemView = mInflater.inflate(
        return new ContactViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.list_contact, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactListAdapter.ContactViewHolder holder, int position) {
        ContactList mCurrent = mContactList.get(position);
        holder.bindTo(mCurrent);
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }


}

