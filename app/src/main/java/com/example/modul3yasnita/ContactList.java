package com.example.modul3yasnita;

public class ContactList {
    private String nama, pekerjaan;
    private int photo;


    public ContactList(String nama, String pekerjaan, int photo){
        this.nama=nama;
        this.pekerjaan=pekerjaan;
        this.photo=photo;

    }

    public String getNama() {

        return nama;
    }

    public String getPekerjaan() {

        return pekerjaan;
    }

    public int getPhoto() {

        return photo;
    }
}
